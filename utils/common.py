#!/usr/bin/env python3

from dataclasses import dataclass
from dataclass_csv import dateformat
from datetime import datetime
from enum import Enum, auto


class Category(Enum):
    NONE = 'None'
    # hardware
    HARDWARE = 'Hardware'
    # Software
    GNOME = 'Gnome Ecosystem'
    MAUI = 'Maui Project'
    NEMO_MOBILE = 'Nemo Mobile'
    CAPYLOON = 'Capyloon'
    MAEMO_LESTE = 'Maemo Leste'
    PHOSH = 'Phosh'
    PLASMA = 'Plasma Ecosystem'
    SAILFISH_OS = 'Sailfish OS'
    UBUNTU_TOUCH = 'Ubuntu Touch'
    RELEASES = 'Releases'
    KERNEL = 'Kernel'
    STACK = 'Stack'
    MATRIX = 'Matrix'
    APPS = 'Apps'
    # distributions
    DISTRIBUTIONS = 'Distributions'
    # Worth Noting
    WORTHNOTING = 'Worth Noting'
    # podcasts 
    PODCASTS = 'Podcasts'
    # video
    VIDEO = 'Video'
    # misc
    MISC = 'Misc'
    TRASH = 'Delete this'


class SourceType(Enum):
    RSS = auto()
    MEGI = auto()
    MSTDN = auto()
    YOUTUBE = auto()


class MimeType(Enum):
    AUDIO = auto()
    VIDEO = auto()
    TEXT = auto()


@dataclass
class Source:
    name: str
    type: SourceType
    url: str
    category: Category = Category.NONE
    MimeType: MimeType = MimeType.TEXT


@dataclass
@dateformat('%Y-%m-%d %H:%M:%S')
class Entry:
    title: str
    url: str
    timestamp: datetime
    source: str
    category: str = Category.MISC.value
