+++
title = "Jolla. (The smartphone, finally.)"
aliases = ["/2013/05/20/jolla-the-smartphone-finally"]
author = "peter"
comments = true
date = "2013-05-23T21:48:00Z"
layout = "post"
[taxonomies]
tags = ["innovation", "Jolla", "MeeGo", "Mer", "platforms", "Sailfish OS", "smartphones"]
categories = ["hardware", "shortform"]
authors = ["peter"]
+++

Jolla, that group of ex-Nokia employees working on an ex-Nokia smartphone OS (MeeGo, which is now "Mer"&mdash;and on the foundations of Mer Jolla built their "Sailfish OS"), have finally announced their first piece of hardware (<a href="http://www.engadget.com/2013/05/20/jolla-phone/">Engadget have the specs</a>), which will ship "at the end of this year"&mdash;you can <a href="https://join.jolla.com/en">pre-order it now!</a>. It's nothing too funky, hardware wise, but Jolla is about software, anyway.

Make sure to watch this first [Hands On video with engadget](https://www.engadget.com/2013-05-20-jolla-phone-hardware-tour.html)!

