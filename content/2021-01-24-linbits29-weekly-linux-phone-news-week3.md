+++
title = "LinBits 29: Weekly Linux Phone news / media roundup (week 3)"
aliases = ["2021/01/24/linbits29-weekly-linux-phone-news-week3.html", "linbits29"]
author = "Peter"
date = "2021-01-24T22:42:00Z"
layout = "post"
[taxonomies]
authors = ["peter"]
tags = ["Pine64", "PinePhone", "Purism", "Librem 5", "squeekboard", "Phosh", "Manjaro", "Plasma Mobile", "JingOS", "Lomiri", "postmarketOS", "fingerprint reader", "hardware accessories"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

Plenty distro releases, a new PinePhone accessory, Phosh 0.8.0 and more. _Commentary in italics._
<!-- more -->

### Software development and releases
* Phosh 0.8.0 [has been released](https://social.librem.one/@agx/105582300169436480). Go read the full [release notes](https://source.puri.sm/Librem5/phosh/-/releases/v0.8.0). _The scaling feature in Settings > Display is really nice. I hope that we'll see fractional scaling there next._
* Squeekboard 1.12.0 "Convolution" has been released, check out the [changelog](https://source.puri.sm/Librem5/squeekboard/-/blob/5b3c185a16daccb73cb3c870c677404215ceafd6/debian/changelog) for which new layouts have been added.
* [postmarketOS have released a Service Pack](https://postmarketos.org/blog/2021/01/22/v20.05.1-is-released/) for their stable v20.05 release. _This is going to help everybody who decided to stick with stable and is going to improve the experience a lot. Still: You're using not the latest software on a nascent platform. Why?_
* openSUSE have released [new images](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/) &mdash; read the  [mailing list post](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/U6SKQJFFRAXKP7CEEHXSYTPBCNPGXSL2/) _Quite interesting, they are going to publish Plasma Mobile images soon (or I just did not find them yet)!_
* Manjaro have pushed out three releases this week: 
  * Phosh has been bumped to [Beta 5](https://forum.manjaro.org/t/manjaro-arm-beta5-with-phosh-pinephone/49917),
  * [Plasma Mobile Beta 2](https://forum.manjaro.org/t/manjaro-arm-beta2-with-plasma-mobile-pinephone/49949) is out,
  * and [Lomiri is now at Alpha 3](https://forum.manjaro.org/t/manjaro-arm-alpha3-with-lomiri-pinephone/49923). _It bugs me a bit that they ship their beta images with ssh enabled and root (password root) active._
* DanctNIX/Arch Linux ARM [have released new images](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20210124). If you already have it installed, upgrading packages it enough.


### Worth reading 

* librefree.me: [Goodbye Addictive Spy Brick, Hello Freedom!](https://librefree.me/goodbye-addictive-spy-brick-hello-freedom) _I believe the headline should rather read: "Hello, addictive Linux Phone!"_
* LinuxSmartphones: [First postmarketOS service pack brings major PinePhone improvements](https://linuxsmartphones.com/the-first-postmarketos-service-pack-release-brings-major-pinephone-improvements/). _If you are running the pre-installed postmarketOS (or for another option opted for postmarketOS stable instead of edge, you'll have an update now that will improve things._
* LinuxSmartphones: [$1 PinePhone pogo pin breakout board lets you connect add-ons without removing the back cover](https://linuxsmartphones.com/1-pinephone-pogo-pin-breakout-board-lets-you-connect-add-ons-without-removing-the-back-cover/). _PINE64 silently launched something nice for makers and tinkerers!_
* /u/zschroeder6212: [Fingerprint scanner update: The new prototype is complete, and it is *MUCH* slimmer!](https://teddit.net/r/PINE64official/comments/l40esv/fingerprint_scanner_update_the_new_prototype_is/). _Awesome progress!_
* LinuxSmartphones: [PinePhone news roundup (1-24-2021)](https://linuxsmartphones.com/pinephone-news-roundup-1-24-2021/). _Another PinePhone roundup from today, lol._
* Purism: [Reflashing the Librem 5](https://puri.sm/posts/reflashing-the-librem-5/). _If you have your Librem 5 and want to give it to someone else, this might help. I am still waiting for that email to confirm my address, though._
* Gamey:  [Fractal on the Pinephone + Encryption and Keyring workaround](https://lbry.tv/@gamey:c/Fractal-on-the-Pinephone:5). _Nice write-up on how to properly run Fractal, the GTK-native matrix client._
* TuxPhones: [New project aims to bring Linux mobile devices back to the mainstream market](https://tuxphones.com/linux-distro-jingos-phone-qt-touch-friendly-jingotab/). _Nice article on JingOS, the iPadOS-like Linux. It's an admirable effort, and I hope it'll go well!_
* FOSS2go: [Mobian Community Edition (CE) PinePhone is available for pre-order in the Pine Store](https://foss2go.com/mobian-community-edition-ce-pinephone-is-available-for-pre-order-in-the-pine-store/). _You can buy this now!_
  * DebugPoint:  [Debian based PinePhone Mobian Edition Review. Spec, Price, and More.](https://www.debugpoint.com/2021/01/pinephone-mobian-edition-review-1/). _Isn't it wonderful to review things before they have even been produced?_
* Nico's blog: [PineTime on SailfishOS](https://www.ncartron.org/pinetime-on-sailfishos.html). _The awesome thing is: You can use Amazfish on other PinePhone distributions too, Manjaro even has it packaged up._
* UBports: [Ubuntu Touch Q&A 92](https://ubports.com/blog/ubport-blogs-news-1/post/ubuntu-touch-q-a-92-3738). _The written and audio equivalents of last weeks stream._


### Worth listening
* postmarketOS Podcast: [#2 Roadmap, daily driving and how to help](https://cast.postmarketos.org/episode/02-Roadmap-daily-driving-and-how-to-help/). _Give it a listen, it's awesome!_


### Worth watching

* ShortCircuit: [Is Linux always the answer? - Librem 5 Smartphone](https://youtube.com/watch?v=BH8DRyKUZDg). _Of course Linux is always the answer!_
* Tech Pills: [My experience with the PinePhone](https://www.youtube.com/watch?v=geiHgB5cJcA). _Awesome video that explains the PinePhone from start to finish. The guy really knows what he is talking about, you may have used one of his apps, e.g. Giara, the Reddit client._
* Tech Show MX: [Probando un sensor térmico en el PinePhone](https://www.youtube.com/watch?v=5LOCMUfuHB4). _Pogo pins in use!_
* Prawnboy SG: [Pinephone - Continuous music with no cuts off even on spotify running inside anbox.](https://www.youtube.com/watch?v=ixtrByju3cU). _You may ask: Why is this worth a video? Well, it was broken for quite a while, and now it works again._
* Elatronion: [PinePhone Manjaro Lomiri: The User Friendly Nerdy OS](https://www.youtube.com/watch?v=I8piUTA2HMs). _Awesome video on the same Manjaro Lomiri release I "showcased" last week._
* Dark1 Linux, Tech, Gaming: [BEST Ubuntu Touch Apps for the PinePhone are …](https://www.youtube.com/watch?v=DilhflSH4pY). _It would have been lovely to see the apps in use on the Phone, too, but since I did not know all the apps he talks about in this video, it might be a worth a watch for you, too._
* Linux Lounge: [A Look At Sailfish OS On The PinePhone](https://peertube.co.uk/videos/watch/9ac64445-eb1e-4cc1-a1d3-4c00d72e02cc). _A nice video on SailfishOS!_
* Leszek Lesner: [SailfishOS for Laptops (x86-64)](https://youtu.be/k6io9J2bPDQ). _If you want to run Sailfish on your laptop, watch this video to find out more!_

### Stuff I did

#### Content

I made one new video: "Getting started with the KDE Community Edition Smartphone"
  * [PeerTube](https://devtube.dev-wiki.de/videos/watch/e32832c3-55e5-4483-b906-5164be4c7839),
  * [LBRY](https://lbry.tv/@linmob:3/getting-started-with-the-kde-community:1),
  * and [YouTube](https://www.youtube.com/watch?v=1uUMiNS5rlw).
  
I also published a matching [blog post](https://linmob.net/2021/01/23/getting-started-with-the-kde-ce-pinephone.html), that tries to cover the parts that I did not cover in the video. _I have also tried to use Plasma Mobile in the past two days for all my PinePhone needs, and while it has made a lot of progress (text input in GTK apps works reliably now), I would not recommend it for daily driving yet &mdash; not only was I unable to get mobile data to work, but receiving and placing calls hasn't exactly been seemless either and Firefox still flickers. I start to think that now that Plasma Mobile made the PinePhone their primary device and dropped libhybris devices, it might be time to drop Ofono in favour of Modem Manager to get more reliable phone calls and more the easy way by following upstream._

#### Random

I also played with [USB-C hardware](https://fosstodon.org/@linmob/105606361446961877). I will try to add my findings to the Pine64 Wiki and finally publish my long drafted blog post on convergence.

#### LINMOBapps

__Poll results:__ I count a total of 20 votes and got overwhelming acceptance for every proposal. This means that further integration of blog and app list are going to happen as soon as I get to it &mdash; my schedule is packed, so more than links in the navigation are unlikely to happen for now. Also, I'll likely add ways to donate cryptocurrencies by simply setting up wallets and providing addresses to donate to. I'll also need to read up on mirroring Git repos properly in order to increase visibility of the list and maybe obtaining drive-by contributions, especially on GitHub. _Stay tuned and remember that your ideas are always very welcome!_

That aside, [a few additions and some maintenance happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Thanks to The3DmaN for his awesome mail-in contribution of 4 apps!  Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)!
