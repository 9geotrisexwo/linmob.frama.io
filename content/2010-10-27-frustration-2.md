+++
title = "Frustration"
aliases = ["2010/10/frustration.html"]
date = "2010-10-27T23:16:00Z"
layout = "post"
[taxonomies]
tags = [ "frustration", "impressions", "mobile computing", "mobile devices"]
categories = ["personal"]
authors = ["peter"]
+++
I've been preparing to publish something here for quite some time. I started a writeup on smartphone platforms while sitting in the train on the way back from an useless job interview in Munich. I didn't finish it, had other things to do, my parents were on holidays nearby, and I had another stupid test to pass. While I was working on that, I had to learn the hard way why exactly Apple came up with their Magsafe stuff: The power supply of my netbook somehow managed to pull it down when I stood up from my desk. Screen broken. Nice black area on the right of the screen. I most likely won't fix it. I will rather sell it, and buy something that's more helpful and an additional battery for my 14&#8221; notebook.
<!-- more -->
In addition to that, another semester has started. As I hate printing all the stuff you need to be prepared at lectures and later be prepared at the examination, simply because it takes so long, is an effing waste of natural resources and it's a pain in the neck to carry all that paper along whenever you travel in order to be able to work, I tried to find something that could fit my workflow. An electronic device, like Amazons kindle, plus some features to be able to make annotations during lectures. I hear someone shouting TabletPC. Sorry, that's not it. It can do too much else and thus distract me badly, it's heavy, expensive and in addition to that, most TabletPCs have a terrible battery life.

And don't talk me into the iPad or an Android tablet. They seem (if my inquiries weren't too bad) to lack the appropriate software - not to mention their sunlight readability. It's too bad that there are so few tablets using Wacom technology. 

Really, the only device that's close to fit and available is made by Apple, and I am not talking of any iOS device here. I think of getting one of these crazy Newton MessagePads. Unfortunately, those that are (relatively) powerful and thus interesting (2000, 2100) are rare and still expensive, and then there is another drawback: Apparently no PDF support. PDF just wasn't that popular in the 1990s. Converting is possible, maybe it would even work out.

But really: Isn't it effing frustrating that there is no effing device that is really suitable to survive University without having to carry a huge load of paper? Is this market so uninteresting? 

Maybe I should turn this disaster into a business plan.

(<a href="https://brimborium.net/frustration">Crossposting, originally posted at brimborium.net</a>)
