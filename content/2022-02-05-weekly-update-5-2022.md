+++
title = "Weekly Update (5/2022): Tow-Boot, GStreamer 1.20, PinePhone Pro Unboxings, an UBports departure and don't miss virtual FOSDEM!"
date = "2022-02-05T11:16:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","PinePhone Pro","Ubuntu Touch","GStreamer","Tow-Boot",]
categories = ["weekly update"]
authors = ["peter"]
+++

Tow-Boot for the PinePhone Pro, two PinePhone Pro development progress reports by Megi, PINE64 PinePhone Community Poll results, mixed news around JingOS and JingPad and plenty PinePhone Pro unboxings!

<!-- more --> _Commentary in italics._ 

### Worth noting
* Sadly, PinePhone malware being spread in chats again[^1]. Fortunately, [release 0.5.5 of Biktorgj's modem firmware](https://github.com/Biktorgj/pinephone_modem_sdk/releases) is working amazingly well for me (I had to reboot for connectivity issues once in the past week, but that was to get WiFi working again) – so go ahead and fix your vulnerable phones by running more open source software on the phone inside your phone! 
* If you're reading this on February 5th or 6th, [head over to FOSDEM's FOSS on Mobile Devices devroom now](https://fosdem.org/2022/schedule/track/foss_on_mobile_devices/)!
* Apologies for being a day late! Don't ask to get your money back :)

### Hardware news
* Apparently, the Planet Computers Astro Slide, successor to the Gemini PDA and Cosmo Communicator, [is now shipping](https://liliputing.com/2022/02/lilbits-holo-lens-canceled-astro-slide-smartphone-with-a-keyboard-now-shipping-and-more.html).

### Hardware enablement news
* megi's PinePhone Development Log: [Pinephone Pro battery charging](https://xnux.eu/log/#059).
* megi's PinePhone Development Log: [Pinephone Pro – Type C port support](https://xnux.eu/log/#060).

### Software news
#### GNOME ecosystem
* This Week in GNOME: [#29 New Year, New Calendar](https://thisweek.gnome.org/posts/2022/02/twig-29/). _I really wonder whether this calendar overhaul is adaptive by default._
#### Plasma/Maui ecosystem
* Volker Krause: [December/January in KDE Itinerary](https://www.volkerkrause.eu/2022/01/29/kde-itinerary-december-january-2022.html).
#### Ubuntu Touch
* UBports: [New Clickable 7 release](http://ubports.com/blog/ubports-news-1/post/new-clickable-7-release-3801).

#### GStreamer
* GStreamer: [News - GStreamer 1.20.0 new major stable release](https://gstreamer.freedesktop.org/). _It's finally here! Better VPU support incoming!_

#### Distro releases
* Megi has released a newer take on his [p-boot multi boot image](https://xnux.eu/p-boot-demo/) for the PinePhone (via [Liliputing](https://liliputing.com/2022/01/test-15-different-pinephone-operating-systems-with-megis-latest-multi-distro-demo-image.html)). _As usual, please keep in mind: This is for test-driving only. Don't use this in "real life", and don't report bugs you experience with it to the distributions included, since this is not using the kernel these distributions usually come with. Also: This is for the PinePhone, not the PinePhone Pro._
* Dan Johansen: [This month in Manjaro (January 2022)](https://blog.strits.dk/this-month-in-manjaro-january-2022/).

### Worth reading

#### PinePhone usage reports
* PINE64: [PinePhone community poll results](https://www.pine64.org/2022/01/31/pinephone-community-poll-results/). _This is definitely worth a read, although the results weren't exactly surprising to me._
* It's FOSS News: [I Used Linux-Based PinePhone Daily For A Year. Here’s What I Learned!](https://news.itsfoss.com/pinephone-review/).
* u/bloggerdan: [Daily Driving Plasma Mobile Part 3](https://www.teddit.net/r/PinePhoneOfficial/comments/sksfhq/daily_driving_plasma_mobile_part_3/).

#### Linux Phones Enthusiasm
* Mobian Blog: [Game Changers](https://blog.mobian-project.org/posts/2022/02/04/game-changers/). _A must read about the PinePhone Pro!_

#### XMPP
* profanity development blog: [Profanity on Pinephone](https://profanity-im.github.io/blog/post/profanity-on-pinephone/).

#### Tablet corner
* TuxPhones: [CutiePi Tablet review: The open-hardware Linux tablet](https://tuxphones.com/cutiepi-review-open-hardware-linux-tablet-raspberry-pi-4/)
* Liliputing: [Makers of the JingPad A1 are selling the Linux tablet for 45-percent off following staffing cuts](https://liliputing.com/2022/02/makers-of-the-jingpad-a1-are-selling-the-linux-tablet-for-45-percent-off-following-staffing-cuts.html)
* Jonah Brüchert: [Debian on an x86_64 tablet](https://jbb.ghsq.ga/2022/02/04/Debian-on-x86-tablet.html)

#### PinePhone Keyboard
* Garbage Collector: [Pinephone Keyboard Case](https://blog.zedas.fr/posts/pinephone-keyboard-case/). _Good write-up, matches up with my experience!_

#### Distro Progress
* Dan Johansen: [This month in Manjaro (January 2022)](https://blog.strits.dk/this-month-in-manjaro-january-2022/).

#### Competition ;-)
* FOSSphones: [Linux Phone News - February 1, 2022](https://fossphones.com/feb-1-22.html).

### Worth listening
* postmarketOS podcast: [#13 EG25-G + fwupd, unudhcpd, mainline.space, OP9, iwd](https://cast.postmarketos.org/episode/13-EG25G-fwupd-unudhcpd-mainline.space-OP9-iwd/). _Another great episode!_

### Worth watching

Again: If you're reading this on February 5th or 6th, [head over to FOSDEM's FOSS on Mobile Devices devroom now](https://fosdem.org/2022/schedule/track/foss_on_mobile_devices/)! Also, don't miss this [talk on Genode on the PinePhone](https://fosdem.org/2022/schedule/event/nfeske/)!

#### Tow Boot on PinePhone Pro
* Martijn Braam: [postmarketOS and the PinePhone Pro with Tow-Boot](https://spacepub.space/w/9YzJkk5g4jbaeQ2bjSgwkP). _Great progress!_
* Martijn Braam: [Tow-Boot installer on the PinePhone Pro](https://spacepub.space/w/s9qs9QoKo4PJdiy1YNPRKr). 

#### PinePhone Pro
* Ambro's: [Pinephone Pro: Early Review and Thoughts at Release](https://www.youtube.com/watch?v=RqThlnxYKsQ). _Nice video, but overly optimistic time frame._
* Nicholas Henkey: [Pinephone Pro Explorer Unboxing and Initial Thoughts](https://www.youtube.com/watch?v=6PDwKomBgaE).
* Rachel Singh: [Checking out my new PinePhone Pro Explorer Edition! :O](Checking out my new PinePhone Pro Explorer Edition! :O).
* Rachel Singh: [How to charge and boot the PinePhone Pro when the battery is drained and the phone won't start](https://www.youtube.com/watch?v=VEJEt4ejLgU).
* BlandManStudios: [Unboxing the PinePhone Pro – Linux Smartphone in 2022](https://www.youtube.com/watch?v=oGnUn2sFkVA). _I like the enthusiasm!_
* \*Mike: [Unboxing the PinePhone Pro Explorer Edition](https://www.youtube.com/watch?v=a5WwxX8AT-o). 
* LINMOB.net: [Unboxing the PinePhone Pro Explorer Edition](https://www.youtube.com/watch?v=h6C2UatW8tQ). 

#### Game Dev on PinePhone Pro
* Rachel Singh: [Pickin' Sticks game (C++/SFML) on PinePhone Pro! 💙](https://www.youtube.com/watch?v=GsP3sFhxdcQ).

#### Ubuntu Touch
* UBports: [Ubuntu Touch Q&A 116](https://www.youtube.com/watch?v=1OGK2lIV8DQ). _Some big news in here with Dalton taking a break. Thanks for all your hard work and take your time, Dalton!_

#### More Ubuntu Touch
* Lumpology: [Installing Ubuntu Touch on Google Pixel 3a XL](https://www.youtube.com/watch?v=v3nZSKsedr4).	
* Nick Shulhin: [Ubuntu Touch on Google Pixel 2 DESTROYS PinePhone | I AM SHOCKED](https://www.youtube.com/watch?v=xy2AjfRKqOY). _I don't know what's so shocking about the Snapdragon 835 being better at being a smartphone SoC than the Allwinner A64, but: Nice vid!_
* Nick Shulhin: [SMS, calls, cellular, wifi - everything WORKS! | Ubuntu Touch Pixel 2](https://www.youtube.com/watch?v=Akf8VJ68nP0).
* Nick Shulhin: [Installing Ubuntu Touch on your 'supported' Android device | UBPorts installer](https://www.youtube.com/watch?v=950bxD6X6Ok).
* Nick Shulhin: [How to SSH into your Ubuntu Touch device | Tutorial](https://www.youtube.com/watch?v=7fdrnU9QMC8).
* Nick Shulhin: [Here is why Ubuntu Touch is still relevant | Answering comments](https://www.youtube.com/watch?v=QJZ6oYHiFZ0).

#### Weird old hardware
* Janus Cycle: [Installing Linux vs Windows 10 Hardware Challenge](https://www.youtube.com/watch?v=ufyh-2vq34E).

#### Shorts
* The Enby Engineer: [DIY Raspberry Pi tablet/cyberdeck computer running KDE Plasma Mobile on PostmarketOS](https://www.youtube.com/watch?v=87fLLoMw72g).
* Angel Vega: [Motorola Droid 4 Maemo Leste glxgears es2gears performance on i3](https://www.youtube.com/watch?v=i4kYBysBq48).

### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

[^1]: Sadly, the [LinuxPhoneApps room](https://matrix.to/#/#linuxphoneapps:matrix.org) was also affected.
